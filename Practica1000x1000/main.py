import time
import os

# Función para buscar el archivo en el sistema
def find_file(filename, search_path):
    for root, dirs, files in os.walk(search_path):
        if filename in files:
            return os.path.join(root, filename)
    return None

# Función para leer el archivo TXT y almacenar los datos en una matriz
def read_txt(file_path):
    matrix = []  # Lista vacía para almacenar las filas de datos 
    with open(file_path, mode='r', encoding='utf-8') as file:  # Abre el archivo en modo lectura con codificación UTF-8
        lines = file.readlines()  # Lee todas las líneas del archivo
        for line in lines:  # Itera sobre todas las líneas
            row = list(map(int, line.strip().split()))   # Divide cada línea en una lista de números enteros, separadas por espacios
            matrix.append(row)  # Agrega la lista de valores a la matriz
    return matrix  # Devuelve los encabezados y la matriz de datos

def buscar_numero_inicio(numero, matrix):
    return matrix[0][0] == numero  # Verifica si el número de inicio coincide con el primer número de la lista
def buscar_numero_medio(numero, matrix):
    medio = len(matrix) // 2  # Calcula el índice de la mitad
    return matrix[medio][medio] == numero  # Compara el valor en la posición de la mitad con n
def buscar_numero_fin(numero, matrix):
    # Obtiene el número de filas y columnas de la matriz
    filas = len(matrix)
    columnas = len(matrix[0])
    # Verifica si el número está en la última posición de la matriz
    return matrix[filas-1][columnas-1] == numero
def probabilidad_no_estar_60(numero, matrix): #Si un numero n tiene una probabilidad del 60% de no estar en la matriz
    contador=0
    respuesta=''
    # Verifica si el número no está en la matriz
    for i in range(len(matrix)): # Itera sobre las filas de la matriz
        for j in range(len(matrix[0])): # Itera sobre las columnas de la matriz
            if matrix[i][j] != numero: # Verifica si el número no coincide con el valor en la posición (i, j)
                contador=contador+1 #Realiza un conteo de todas las veces que el número no coincide con la matriz
    probabilidad=contador/(len(matrix)*len(matrix[0]))
    if probabilidad>0.6:
        respuesta='No, es mayor, la probabilidad es de '+str(probabilidad*100)+'%'
    elif probabilidad==0.6:
        respuesta='Sí, la probabilidad es de '+str(probabilidad*100)+'%'
    else:
        respuesta='No, es menor, la probabilidad es de '+str(probabilidad*100)+'%'
    return respuesta

archivo = find_file('datoss.txt', os.getcwd())
matrix = read_txt(archivo)

numero = int(input("Ingresa un número: "))  # Convertir la entrada a entero
time1_inicio = time.time()
inicio=buscar_numero_inicio(numero, matrix)
time1_fin = time.time()
time1 = time1_fin - time1_inicio
time2_inicio = time.time()
medio=buscar_numero_medio(numero, matrix)
time2_fin = time.time()
time2 = time2_fin - time2_inicio
time3_inicio = time.time()
fin=buscar_numero_fin(numero, matrix)
time3_fin = time.time()
time3 = time3_fin - time3_inicio
time4_inicio = time.time()
probabilidad=probabilidad_no_estar_60(numero, matrix)
time4_fin = time.time()
time4 = time4_fin - time4_inicio

# Mostrar el tiempo que demora cada caso y el resultado de la función
print(str(numero) + ' En la primera posición: '+str(inicio))
print('Tiempo de ejecución: ' + str(time1) + ' segundos')
print ('El orden de magnitud de la funcion es: O(1) ya que acceden a una posición en la matriz y sólo se ejecuta una vez')
print(str(numero) + ' En la mitad: '+str(medio))
print('Tiempo de ejecución: ' + str(time2) + ' segundos')
print ('El orden de magnitud de la funcion es: O(1) ya que acceden a una posición en la matriz y sólo se ejecuta una vez')
print(str(numero) + ' En la última posición: '+str(fin))
print('Tiempo de ejecución: ' + str(time3) + ' segundos')
print ('El orden de magnitud de la funcion es: O(1) ya que acceden a una posición en la matriz y sólo se ejecuta una vez')
print(str(numero) + ' ¿Con una probabilidad de no escontrarlo del 60%?: '+str(probabilidad))
print('Tiempo de ejecución: ' + str(time4) + ' segundos')
print ('El orden de magnitud de la funcion es:O(m*n)+O(1) ya que recorre toda la matriz filas y columnas, realiza una operacion y accede al condicional')


