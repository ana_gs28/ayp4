from funciones import *
import tkinter as tk
from tkinter import messagebox, ttk

# Lectura del archivo TXT
file_path = find_file('datosUsuarios.txt', os.getcwd())
headers, datosUsers = read_txt(file_path)
numeroDatos = len(datosUsers)

btree = BTree()
toke = Token()
toke.generar_tokens(numeroDatos)

for index, row in enumerate(datosUsers):
    tipo = row[0]
    documento = tipo_doc(tipo, row[1])
    nombre = row[2]
    token = toke.asignar_token(documento)
    contrasena = password(row[4], index)
    fecha_nac = row[5]
    ciudad = row[6]
    correo = verif_correo(row[7])
    btree.insert(tipo, documento, nombre, token, contrasena, fecha_nac, ciudad, correo, index)

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.label_cedula = tk.Label(self, text="Cédula:")
        self.label_cedula.pack()

        self.entry_cedula = tk.Entry(self)
        self.entry_cedula.pack()

        self.label_correo = tk.Label(self, text="Correo:")
        self.label_correo.pack()

        self.entry_correo = tk.Entry(self)
        self.entry_correo.pack()

        self.label_password = tk.Label(self, text="Contraseña:")
        self.label_password.pack()

        self.entry_password = tk.Entry(self, show="*")
        self.entry_password.pack()

        self.button_login = tk.Button(self, text="Login", command=self.loginTK)
        self.button_login.pack()

        self.button_recordar = tk.Button(self, text="Recordar Contraseña", command=self.recordarContrasenaTK)
        self.button_recordar.pack()

    def loginTK(self):
        cedula = self.entry_cedula.get()
        contrasena = self.entry_password.get()
        user_data = login(cedula, contrasena, btree, datosUsers)
        if user_data:
            messagebox.showinfo("Login", "Login exitoso!")
            self.show_user_options(cedula)
        else:
            messagebox.showerror("Error", "Cédula o contraseña incorrectos.")

    def recordarContrasenaTK(self):
        cedula = self.entry_cedula.get()
        correo = self.entry_correo.get()
        contrasena = recordarContrasena(cedula, correo, btree)
        if contrasena:
            messagebox.showinfo("Recordar Contraseña", f"Su contraseña es: {contrasena}")
        else:
            messagebox.showerror("Error", "Si desea recuperar la contraseña, ingrese la cedula y el correo")
            messagebox.showerror("Error", "El correo no coincide con la cédula")

    def show_user_options(self, cedula):
        self.clear_widgets()
        self.label_option = tk.Label(self, text="Opciones:")
        self.label_option.pack()

        self.button_token = tk.Button(self, text="Ver Token", command=lambda: self.ver_token(cedula))
        self.button_token.pack()

        self.button_modificar = tk.Button(self, text="Modificar Contraseña", command=lambda: self.modificar_contrasena(cedula))
        self.button_modificar.pack()

        self.button_logout = tk.Button(self, text="Cerrar Sesión", command=self.logout)
        self.button_logout.pack()

    def ver_token(self, cedula):
        user_token = btree.search(cedula)
        if user_token:
            messagebox.showinfo("Token", f"Token: {user_token['token']}")
        else:
            messagebox.showerror("Error", "No se pudo encontrar el token")

    def modificar_contrasena(self, cedula):
        self.clear_widgets()
        headers = ['Cédula', 'Tipo de documento', 'Nombre', 'Fecha de nacimiento', 'Ciudad', 'Correo']
        data = btree.mostrar()
        
        self.tree = ttk.Treeview(self, columns=headers, show='headings')
        for header in headers:
            self.tree.heading(header, text=header)
            self.tree.column(header, width=100)
        for row in data:
            self.tree.insert('', tk.END, values=row)
        self.tree.pack()

        self.label_new_password = tk.Label(self, text="Nueva Contraseña:")
        self.label_new_password.pack()

        self.entry_new_password = tk.Entry(self, show="*")
        self.entry_new_password.pack()

        self.button_save_password = tk.Button(self, text="Guardar", command=lambda: self.save_new_password(cedula))
        self.button_save_password.pack()

    def save_new_password(self, cedula):
        new_password = self.entry_new_password.get()
        user_info = btree.search(cedula)
        if user_info:
            user_info['contrasena'] = new_password
            btree.update(cedula, user_info)
            messagebox.showinfo("Éxito", "Contraseña actualizada con éxito")
            self.show_user_options(cedula)
        else:
            messagebox.showerror("Error", "No se pudo actualizar la contraseña")

    def logout(self):
        self.clear_widgets()
        self.create_widgets()

    def clear_widgets(self):
        for widget in self.winfo_children():
            widget.destroy()

root = tk.Tk()
app = Application(master=root)
app.mainloop()
