import os
import re
import random
import string
import tkinter as tk
from tkinter import messagebox

#Espresiones regulares para validar información
cc="^[0-9]{6,10}$"
passwor="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,16}$"
fecha_nac="^(0[1-9]|1[0-9]|2[0-9]|3[0-1])[- /.](0[1-9]|1[012])[- /.](18[0-9][0-9]|19[0-9][0-9]|20[0-9][0-9])$"
correo="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"
correoEst = "^[a-zA-Z0-9_.+-]+@elpoli[.]edu[.]co+$"

# Función para buscar el archivo en el sistema
def find_file(filename, search_path):
    for root, dirs, files in os.walk(search_path):
        if filename in files:
            return os.path.join(root, filename)
    return None

# Función para leer el archivo TXT y almacenar los datos en una matriz
def read_txt(file_path):
    matrix = []  # Lista vacía para almacenar las filas de datos
    with open(file_path, mode='r', encoding='utf-8') as file:  # Abre el archivo en modo lectura con codificación UTF-8
        lines = file.readlines()  # Lee todas las líneas del archivo
        headers = lines[0].strip().split(',')  # La primera línea contiene los encabezados, se separan por comas
        for line in lines[1:]:  # Itera sobre las líneas restantes (los datos)
            row = line.strip().split(',')  # Divide cada línea en una lista de valores, separadas por comas
            matrix.append(insertar_matriz(row))  # Agrega la lista de valores a la matriz
    return headers, matrix  # Devuelve los encabezados y la matriz de datos

def insertar_matriz(row):
    token=Token()
    token.generar_tokens(20)
    fila=[]
    tipo = row[0]  # Extrae el tipo de documento (suponiendo que es el primer campo)
    documento = tipo_doc(tipo, row[1])  # Extrae la cédula (suponiendo que es el segundo campo)
    nombre = row[2]  # Extrae el nombre (suponiendo que es el tercero campo)
    token=token.asignar_token(documento)
    contrasena = password(row[4], nombre) # Extrae la contraseña (suponiendo que es el cuarto campo) = row[4]  # Extrae la contraseña (suponiendo que es el quinto campo)
    fecha_nac = row[5]  # Extrae la fecha de nacimiento (suponiendo que es el sexto campo)
    ciudad = row[6]  # Extrae la ciudad (suponiendo que es el septimo campo)
    correo = verif_correo(row[7])  # Extrae el correo (suponiendo que es el octavo campo)
    fila.append(tipo, documento, nombre, token, contrasena, fecha_nac, ciudad, correo)  # Agrega la lista de valores a la lista
    return fila
def mostrar_matriz(matrix):
    data=[]
    for row in matrix:
        #mostrar tipo, documento, nombre, fecha_nac, ciudad, correo
        data=[row[0],row[1],row[2],row[5],row[6],row[7]] #Extrae campos especificos de la matriz
    return data

# Clase BTree para almacenar contraseñas
class BTree:
    def __init__(self):
        self.tree = {}  # Inicializa un diccionario vacío para almacenar los datos del árbol B
    
    def insert(self,cedula,contrasena,row_number):
        # Inserta un nuevo elemento en el árbol B
        # La clave es la cédula y el valor es un diccionario con la contraseña y el número de fila
        self.tree[cedula] = {'contrasena': contrasena, 'row_number': row_number}
    
    def search(self, cedula):
        # Busca una cédula en el árbol B
        # Si se encuentra, devuelve el diccionario con la contraseña y el número de fila, de lo contrario, devuelve None
        return self.tree.get(cedula, None)
    
    def update(self, cedula, new_info):
        # Actualiza la información de un usuario existente en el árbol B
        self.tree[cedula] = new_info
        print("Informacion actualizada con exito")

# Función de login
def login(cedula, contrasena, btree, matrix):
    user_info = btree.search(cedula)  # Busca la cédula en el árbol B
    if user_info and user_info['contrasena'] == contrasena:  # Verifica si la cédula existe y la contraseña coincide
        row_number = user_info['row_number']  # Obtiene el número de fila de la matriz
        user_data = matrix[row_number]  # Obtiene los datos del usuario de la matriz
        return user_data  # Devuelve los datos del usuario
    return None  # Si la cédula no existe o la contraseña no coincide, devuelve None
def recordarContrasena(documento, correo, matrix):
    for row in matrix:  #Itera sobre la matriz
        if row[1] == documento and row[7] == correo: #Verifica si la cedula y el correo coinciden
            return row[4] #Retorna la contrasena
        else :
            print("El correo no coincide con la cedula")
            return None
def tipo_doc(tipo_doc, documento):
    if tipo_doc == 'CC':#Valida si el tipo de documento es CC
        validation=re.findall(cc, documento) #Verifica que sea un número que cumpla con la expresion regular
        if validation: #Si cumple con la expresion regular
            return documento #Retorna el documento
        else: #Si no cumple con la expresion regular
            print("El documento no es valido, deben ser números") #Muestra un mensaje de error
            return None #Retorna None
    else: #Si el tipo de documento no es CC
        return documento #Retorna el documento
    
def password(contrasena, nombre):
    validation=re.findall(passwor, contrasena) #Verifica que sea una contraseña que cumpla con la expresion regular
    if validation: #Si cumple con la expresion regular
        return contrasena #Retorna la contraseña
    else: #Si no cumple con la expresion regular
        print("La contraseña de " + str(nombre) + " no es valida, debe tener al menos 8 caracteres, una mayúscula, una minúscula, un número y un caracter especial") #Muestra un mensaje de error
        return None #Retorna None
    
def fecha_nacimiento(fecha_nacimiento):
    validation=re.findall(fecha_nac, fecha_nacimiento) #Verifica que sea una fecha que cumpla con la expresion regular
    if validation: #Si cumple con la expresion regular
        return fecha_nacimiento #Retorna la fecha de nacimiento
    else: #Si no cumple con la expresion regular
        print("La fecha de nacimiento no es valida, debe ser dd/mm/aaaa o dd-mm-aaaa o dd.mm.aaaa") #Muestra un mensaje de error
        return None #Retorna None
    
def verif_correo(correo):
    validation=re.findall(correo, correo) #Verifica que sea un correo que cumpla con la expresion regular
    validationEst=re.findall(correoEst, correo) #Verifica que sea un correo que cumpla con la expresion regular
    if validation: #Si cumple con la expresion regular
        if validationEst: #Si cumple con la expresion regular
            print("Es un correo de estudiante") #Muestra un mensaje 
            return correo #Retorna el correo
        else: #Si no cumple con la expresion regular
            print("Es un correo normal") #Muestra un mensaje
            return correo #Retorna el correo
    else: #Si no cumple con la expresion regular
        print("El correo no es valido") #Muestra un mensaje de error
        return None #Retorna None
class Token:
    def __init__(self):
        self.tokens = set()  # Inicializa un conjunto vacío para almacenar tokens únicos
    
    def generar_token(self):
        # Genera un token aleatorio de 10 caracteres (mayúsculas y dígitos)
        return ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
    
    def generar_tokens(self, n):
        # Genera 'n' tokens únicos
        while len(self.tokens) < n:  # Continúa hasta que el conjunto tenga 'n' tokens únicos
            token = self.generar_token()  # Genera un nuevo token
            if token not in self.tokens:  # Verificación adicional (aunque redundante con el conjunto)
                self.tokens.add(token)  # Añade el token al conjunto (solo se añade si no existe)
    
    def imprimir_tokens(self):
        # Imprime todos los tokens almacenados en el conjunto
        for token in self.tokens:  # Itera sobre cada token en el conjunto
            print(token)  # Imprime el token
    
    def asignar_token(self, user_id):
        # Asigna un token a un usuario
        if len(self.tokens) == 0:  # Verifica si no hay tokens disponibles
            raise ValueError("No hay tokens disponibles. Primero genera tokens.")  # Lanza una excepción si no hay tokens
        token = self.tokens.pop()  # Extrae un token único del conjunto
        return token  # Retorna el token asignado


