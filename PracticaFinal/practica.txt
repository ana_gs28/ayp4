Escribir un programa que gestione claves de usuario, datos de los usuarios se encuentran en un archivo
de texto con los siguientes campos:
Tipo de documento, cédula, nombre, contraseña, token, fecha de nacimiento, ciudad, correo universitario

El programa debe permitir:
Registrar usuarios con las siguientes condiciones: 
-> Si el tipo de documento es cedula, esta debe ser numérica.
-> La contraseña debe ser una clave fuerte.
-> El token se genera con un método automático.
-> La fecha de nacimiento se debe validar como dd/mm/aaaa.
-> Si el correo es el dominio del poli se debe validar como de estudiantes, (el correo puede ser de diferentes dominios).

Para las validaciones se deben utilizar expresiones regulares.

Recuperar contraseña: Si se quiere recuperar la contraseña se debe pedir el documento y el correo, validarlo 
y mostrar la contraseña.

Modificar contraseña:
Simular el ingreso al sistema:
Mostrar todos los usuarios inscritos sin mostrar las contraseñas.
Las contraseñas creadas se deben guardar en un archivo, desde el cual se recupera y se valida la simulacion
de ingreso.
Los datos se almacenan como una matriz y las contraseñas se guardan en un árbol B donde está la cédula y el 
número de fila de la matriz donde está guardado.

CRITERIOS DE EVALUACIÓN
Cumplimiento de requisitos
Interfaz
Sustentación individual
Validaciones


Para el requisito 
Las contraseñas creadas se deben guardar en un archivo, desde el cual se recupera y se valida la simulacion
de ingreso.
# Añadir datos a un archivo existente
with open('archivo.txt', 'a') as file:
    file.write("Añadiendo una nueva línea al archivo.\n")
    file.write("Añadiendo otra línea al archivo.\n")