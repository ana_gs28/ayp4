import os
import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
from funcion import *

# Lectura del archivo TXT
file_path = find_file('datosUsuarios.txt', os.getcwd())
headers, datosUsers = read_txt(file_path)
numeroDatos = len(datosUsers)

# Crear la variable btree para insertar datos en el árbol B *Clase BTree*
btree = BTree()
toke = Token()
toke.generar_tokens(numeroDatos)
for index, row in enumerate(datosUsers):  # Itera sobre la matriz de datos recorre la matriz
    # Inserta los datos en el árbol B
    btree.insert(row[1],row[4],index)  # Inserta en el árbol B la cedula, la contrasena y el indice
class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.label_cedula = tk.Label(self, text="Cédula:")
        self.label_cedula.pack()

        self.entry_cedula = tk.Entry(self)
        self.entry_cedula.pack()

        self.label_correo = tk.Label(self, text="Correo:")
        self.label_correo.pack()

        self.entry_correo = tk.Entry(self)
        self.entry_correo.pack()

        self.label_password = tk.Label(self, text="Contraseña:")
        self.label_password.pack()

        self.entry_password = tk.Entry(self, show="*")
        self.entry_password.pack()

        self.button_login = tk.Button(self, text="Login", command=self.loginTK)
        self.button_login.pack()

        self.button_recordar = tk.Button(self, text="Recordar Contraseña", command=self.recordarContrasenaTK)
        self.button_recordar.pack()
        
        self.button_registrar = tk.Button(self, text="Registrar Usuario", command=self.show_register_options)
        self.button_registrar.pack()

    def loginTK(self):
        cedula = self.entry_cedula.get()
        contrasena = self.entry_password.get()
        user_data = login(cedula, contrasena, btree, datosUsers)
        if user_data:
            messagebox.showinfo("Login", "Login exitoso!")
            self.show_user_options(cedula)
        else:
            messagebox.showerror("Error", "Cédula o contraseña incorrectos.")

    def recordarContrasenaTK(self):    
        cedula = self.entry_cedula.get()
        correo = self.entry_correo.get()
        contrasena = recordarContrasena(cedula, correo, datosUsers)
        if contrasena:
            messagebox.showinfo("Recordar Contraseña", f"Su contraseña es: {contrasena}")
        else:
            messagebox.showerror("Error", "Si desea recuperar la contraseña, ingrese la cedula y el correo")
            messagebox.showerror("Error", "El correo no coincide con la cédula")

    def show_register_options(self):
        self.label_tipoD = tk.Label(self, text="Tipo de documento:")
        self.label_tipoD.pack()

        self.entry_tipoD = tk.Entry(self)
        self.entry_tipoD.pack()

        self.label_documento = tk.Label(self, text="N° documento:")
        self.label_documento.pack()

        self.entry_documento = tk.Entry(self)
        self.entry_documento.pack()

        self.label_nombre = tk.Label(self, text="Nombre:")
        self.label_nombre.pack()

        self.entry_nombre = tk.Entry(self)
        self.entry_nombre.pack()

        self.label_password = tk.Label(self, text="Contraseña:")
        self.label_password.pack()

        self.entry_password = tk.Entry(self, show="*")
        self.entry_password.pack()

        self.label_fecha = tk.Label(self, text="Fecha de nacimiento:")
        self.label_fecha.pack()

        self.entry_fecha = tk.Entry(self)
        self.entry_fecha.pack()

        self.label_ciudad = tk.Label(self, text="Ciudad:")
        self.label_ciudad.pack()

        self.entry_ciudad = tk.Entry(self)
        self.entry_ciudad.pack()

        self.label_correo = tk.Label(self, text="Correo:")
        self.label_correo.pack()

        self.entry_correo = tk.Entry(self)
        self.entry_correo.pack()

        self.button_reg_user = tk.Button(self, text="Registrar", command=lambda: self.registrarUsuarioTK())
        self.button_reg_user.pack()
    def registrarUsuarioTK(self):
        tipo=self.entry_tipoD.get()
        cedula = self.entry_documento.get()
        nombre = self.entry_nombre.get()
        token=0
        contrasena = self.entry_password.get()
        fecha = self.entry_fecha.get()
        ciudad= self.entry_ciudad.get()
        correo = self.entry_correo.get()
        valido=registrarUsuario(tipo, cedula, nombre, token, contrasena, fecha, ciudad, correo, datosUsers, toke, btree)
        if valido:
            messagebox.showinfo("Registro", "Registro exitoso!")
            self.show_user_options(cedula)
        else:
            messagebox.showerror("Error", "Cedula ya registrada")

    def show_user_options(self, cedula):
        self.clear_widgets()
        self.label_option = tk.Label(self, text="Opciones:")
        self.label_option.pack()

        self.button_token = tk.Button(self, text="Ver Token", command=lambda: self.ver_token(cedula))
        self.button_token.pack()

        self.button_modificar = tk.Button(self, text="Modificar Contraseña", command=lambda: self.modificar_contrasena(cedula))
        self.button_modificar.pack()

        self.button_logout = tk.Button(self, text="Cerrar Sesión", command=self.logout)
        self.button_logout.pack()

    def ver_token(self, cedula):
        user_token = []
        for row in datosUsers:  # Itera sobre la matriz de datos
            if row[1] == cedula:  # Verifica si la cedula coincide con la cedula del usuario
                user_token = row
        if user_token:
            messagebox.showinfo("Token", f"Token: {user_token[3]}") # Muestra el token del usuario en caso de que la cedula exista
        else:
            messagebox.showerror("Error", "No se pudo encontrar el token")

    def modificar_contrasena(self, cedula):
        self.clear_widgets()
        headers = ['Cédula', 'Tipo de documento', 'Nombre', 'Fecha de nacimiento', 'Ciudad', 'Correo']
        data = mostrar_matriz(datosUsers)
        
        self.tree = ttk.Treeview(self, columns=headers, show='headings')
        for header in headers:
            self.tree.heading(header, text=header)
            self.tree.column(header, width=100)
        for row in data:
            self.tree.insert('', tk.END, values=row)
        self.tree.pack()

        self.label_new_password = tk.Label(self, text="Nueva Contraseña:")
        self.label_new_password.pack()

        self.entry_new_password = tk.Entry(self, show="*")
        self.entry_new_password.pack()

        self.button_save_password = tk.Button(self, text="Guardar", command=lambda: self.save_new_password(cedula))
        self.button_save_password.pack()

    def save_new_password(self, cedula):
        new_password = self.entry_new_password.get()
        user_info = btree.search(cedula)
        file_path = find_file('passwords.txt', os.getcwd())
        with open(file_path, 'a') as file:
            file.write(cedula + ',' + new_password + '\n')
        if user_info:
            user_info['contrasena'] = new_password
            btree.update(cedula, user_info)
            messagebox.showinfo("Éxito", "Contraseña actualizada con éxito")
            self.show_user_options(cedula)
        else:
            messagebox.showerror("Error", "No se pudo actualizar la contraseña")

    def logout(self):
        self.clear_widgets()
        self.create_widgets()

    def clear_widgets(self):
        for widget in self.winfo_children():
            widget.destroy()

root = tk.Tk()
app = Application(master=root)
app.mainloop()
