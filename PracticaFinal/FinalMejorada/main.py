from funcion import *
import tkinter as tk
from tkinter import messagebox

# Lectura del archivo TXT
file_path = find_file('datosUsuarios.txt', os.getcwd())
headers, datosUsers = read_txt(file_path) # Lee el archivo TXT y obtiene los encabezados y la matriz de datos
print(headers)  # Muestra los encabezados
print(datosUsers)   # Muestra la matriz de los datos de los usuarios como una lista de listas, aca lista con los datos ordenados del archivo.txt excepto los encabezados.

numeroDatos=len(datosUsers)

# Crear la variable btree para insertar datos en el árbol B *Clase BTree*
btree = BTree()
toke = Token()
toke.generar_tokens(numeroDatos)
for index, row in enumerate(datosUsers):  # Itera sobre la matriz de datos recorre la matriz
    # Inserta los datos en el árbol B
    btree.insert(row[1],row[4],index)  # Inserta en el árbol B la cedula, la contrasena y el indice

opcion=''
while opcion!='C':
    # Login ingresando cédula y contraseña
    cedula = input("Ingrese su cédula: ")
    contrasena = input("Ingrese su contraseña: ")
    #Llama a la función login para verificar los datos ingresados y guarda el resultado en la variable user_data
    user_data = login(cedula, contrasena, btree, datosUsers)  # Verifica las credenciales del usuario *funcion login*
    if user_data:
        print("Login exitoso!")
        print(f"Datos del usuario: {user_data}")  # Muestra los datos del usuario en caso de login exitoso
        print("Sesión iniciada.")
        opcion = input("Ver token (T), Modificar contraseña (M), Cerrar sesión (C): ")  # Pregunta al usuario si desea cerrar la sesión
        match opcion:  # Verifica la respuesta del usuario
            case 'T':
                user_token = []
                for row in datosUsers:  # Itera sobre la matriz de datos
                    if row[1] == cedula:  # Verifica si la cedula coincide con la cedula del usuario
                        user_token = row
                print(f"Token: {user_token[3]}")  # Muestra el token del usuario en caso de que la cedula exista
            case 'M':
                mostrar_matriz(datosUsers)
                new_password = input("Ingrese la nueva contraseña: ")  # Pide la nueva contraseña al usuario
                btree.update(cedula, {'contrasena': new_password})  # Actualiza la contraseña en el árbol B
            case 'C':   
                print("Sesión cerrada.")
                exit()
    else:
        print("Cédula o contraseña incorrectos.")  # Mensaje en caso de credenciales incorrectas
        opcion2=input("¿Desea recuperar la contraseña (S/N)? ")
        if opcion2=='S':
            cedulaR = input("Ingrese su cédula: ")
            correoR = input("Ingrese su correo: ")
            print(f"Su contraseña es: {recordarContrasena(cedulaR, correoR, btree)}")
        else:
            opcion='C'
            exit()
