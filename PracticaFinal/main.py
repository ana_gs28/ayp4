from funciones import *
import tkinter as tk
from tkinter import messagebox

# Lectura del archivo TXT
file_path = find_file('datosUsuarios.txt', os.getcwd())
headers, datosUsers = read_txt(file_path) # Lee el archivo TXT y obtiene los encabezados y la matriz de datos
print(headers)  # Muestra los encabezados
print(datosUsers)   # Muestra la matriz de los datos de los usuarios como una lista de listas, aca lista con los datos ordenados del archivo.txt excepto los encabezados.

numeroDatos=len(datosUsers)

# Crear la variable btree para insertar datos en el árbol B *Clase BTree*
btree = BTree()
toke = Token()
toke.generar_tokens(numeroDatos)
for index, row in enumerate(datosUsers):  # Itera sobre la matriz de datos recorre la matriz
    # Inserta los datos en el árbol B
    tipo = row[0]  # Extrae el tipo de documento (suponiendo que es el primer campo)
    documento = tipo_doc(tipo, row[1])  # Extrae la cédula (suponiendo que es el segundo campo)
    nombre = row[2]  # Extrae el nombre (suponiendo que es el tercero campo)
    token=toke.asignar_token(documento)
    contrasena = password(row[4], index) # Extrae la contraseña (suponiendo que es el cuarto campo) = row[4]  # Extrae la contraseña (suponiendo que es el quinto campo)
    fecha_nac = row[5]  # Extrae la fecha de nacimiento (suponiendo que es el sexto campo)
    ciudad = row[6]  # Extrae la ciudad (suponiendo que es el septimo campo)
    correo = verif_correo(row[7])  # Extrae el correo (suponiendo que es el octavo campo)
    btree.insert(tipo,documento,nombre,token,contrasena,fecha_nac,ciudad,correo, index)  # Inserta en el árbol B

opcion=''
while opcion!='C':
    # Login ingresando cédula y contraseña
    cedula = input("Ingrese su cédula: ")
    contrasena = input("Ingrese su contraseña: ")
    #Llama a la función login para verificar los datos ingresados y guarda el resultado en la variable user_data
    user_data = login(cedula, contrasena, btree, datosUsers)  # Verifica las credenciales del usuario *funcion login*
    if user_data:
        print("Login exitoso!")
        print(f"Datos del usuario: {user_data}")  # Muestra los datos del usuario en caso de login exitoso
        print("Sesión iniciada.")
        opcion = input("Ver token (T), Modificar contraseña (M), Cerrar sesión (C): ")  # Pregunta al usuario si desea cerrar la sesión
        match opcion:  # Verifica la respuesta del usuario
            case 'T':
                user_token = btree.search(cedula)  # Busca la cédula en el árbol B
                print(f"Token: {user_token['token']}")  # Muestra el token del usuario en caso de que la cedula exista
            case 'M':
                btree.mostrar()
                new_password = input("Ingrese la nueva contraseña: ")  # Pide la nueva contraseña al usuario
                btree.update(cedula, {'contrasena': new_password})  # Actualiza la contraseña en el árbol B
            case 'C':   
                print("Sesión cerrada.")
                exit()
    else:
        print("Cédula o contraseña incorrectos.")  # Mensaje en caso de credenciales incorrectas
        opcion2=input("¿Desea recuperar la contraseña (S/N)? ")
        if opcion2=='S':
            cedulaR = input("Ingrese su cédula: ")
            correoR = input("Ingrese su correo: ")
            print(f"Su contraseña es: {recordarContrasena(cedulaR, correoR, btree)}")
        else:
            opcion='C'
            exit()
