import memory_profiler
@memory_profiler.profile
def busqueda_binaria(lista, objetivo):
    cont=0
    izquierda = 0  # Inicializa el índice izquierdo al inicio de la lista
    cont=cont+1
    derecha = len(lista) - 1  # Inicializa el índice derecho al final de la lista
    cont=cont+1
    while izquierda <= derecha:  # Mientras el índice izquierdo sea menor o igual al derecho
        medio = (izquierda + derecha) // 2  # Calcula el índice medio
        if lista[medio] == objetivo:  # Si el elemento en el índice medio es igual al objetivo
            return medio  # Retorna el índice del objetivo
        elif lista[medio] < objetivo:  # Si el elemento en el índice medio es menor que el objetivo
            izquierda = medio + 1  # Ajusta el índice izquierdo para buscar en la mitad derecha
        else:  # Si el elemento en el índice medio es mayor que el objetivo
            derecha = medio - 1  # Ajusta el índice derecho para buscar en la mitad izquierda
    cont=cont+1
    print('El contador de frecuencia o iteraciones: ',str(cont))
    return -1  # Retorna -1 si el objetivo no está en la lista



