import time
import random
from BusqVector import *
from BusqArbol import *
import memory_profiler

# Se crea una lista con 60 valores aleatorios y se pide el valor que se desea buscar
lista_valores = [random.randint(1, 150) for _ in range(60)]  # Crea una lista con 60 valores aleatorios entre 1 y 100
objetivo=int(input("Ingresa el valor que deseas buscar: "))

#Ejemplo Vector
print("\nAlgoritmo de búsqueda binaria en un vector:")
inicioV=time.time() # Calcula el tiempo de ejecución al principio del vector
resultado = busqueda_binaria(lista_valores, objetivo)  # Llama a la función de búsqueda binaria
# Información sobre el uso de memoria de la función
print(memory_profiler.memory_usage())
finV=time.time() # Calcula el tiempo de ejecución al final en el vector
if resultado == -1:
    print(f"El elemento {objetivo} no está en la lista")  # Imprime si no se encontró
else:
    print(f"El elemento {objetivo} está en el índice {resultado}")  # Imprime el resultado de la búsqueda
print(f"Tiempo de ejecución: {(finV-inicioV)*1000000000} nanosegundos\n")
# Uso de memoria de la lista grande
print('Uso de memoria: ',memory_profiler.memory_usage(-1, interval=0.1))

#Ejemplo Árbol
print("Algoritmo de búsqueda binaria en un árbol:")
ABB = ArbolBinarioDeBusqueda()  # Crea un nuevo árbol binario de búsqueda
for v in lista_valores:
    ABB.insertar(v)  # Inserta cada valor en el ABB

inicioA=time.time() # Calcula el tiempo de ejecución al principio del árbol
nodo_encontrado = ABB.buscar(objetivo)  # Busca el objetivo en el ABB
# Información sobre el uso de memoria de la función
print(memory_profiler.memory_usage())
finA=time.time() # Calcula el tiempo de ejecución al final en el árbol
if nodo_encontrado:
    print(f"Elemento {objetivo} encontrado con valor: {nodo_encontrado.valor}")  # Imprime si se encontró
else:
    print(f"Elemento {objetivo} no encontrado en el árbol")  # Imprime si no se encontró

print(f"Tiempo de ejecución: {(finA-inicioA)*1000000000} nanosegundos")
print('Uso de memoria: ',memory_profiler.memory_usage(-1, interval=0.1))
