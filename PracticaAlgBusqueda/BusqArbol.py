import memory_profiler
class Nodo:
    def __init__(self, valor):
        self.valor = valor  # Asigna el valor al nodo
        self.izquierda = None  # Inicializa el hijo izquierdo como None
        self.derecha = None  # Inicializa el hijo derecho como None

class ArbolBinarioDeBusqueda:
    def __init__(self):
        self.raiz = None  # Inicializa la raíz del árbol como None

    def insertar(self, valor):
        if self.raiz is None:  # Si la raíz es None, crea un nuevo nodo raíz
            self.raiz = Nodo(valor)
        else:
            self._insertar(valor, self.raiz)  # Llama al método privado _insertar

    def _insertar(self, valor, nodo):
        if valor < nodo.valor:  # Si el valor es menor que el valor del nodo
            if nodo.izquierda is None:  # Si no hay hijo izquierdo, crea uno nuevo
                nodo.izquierda = Nodo(valor)
            else:
                self._insertar(valor, nodo.izquierda)  # Llama recursivamente a _insertar en el subárbol izquierdo
        else:  # Si el valor es mayor o igual que el valor del nodo
            if nodo.derecha is None:  # Si no hay hijo derecho, crea uno nuevo
                nodo.derecha = Nodo(valor)
            else:
                self._insertar(valor, nodo.derecha)  # Llama recursivamente a _insertar en el subárbol derecho

    def buscar(self, valor):
        return self._buscar(valor, self.raiz)  # Llama al método privado _buscar
    @memory_profiler.profile
    def _buscar(self, valor, nodo):
        cont=0
        if nodo is None:  # Si el nodo es None, el objetivo no está en el árbol
            return None
        cont=cont+1
        if nodo.valor == valor:  # Si el valor del nodo es igual al objetivo, se encontró
            return nodo
        cont=cont+1
        if valor < nodo.valor:  # Si el valor es menor que el valor del nodo
            return self._buscar(valor, nodo.izquierda)  # Buscar en el subárbol izquierdo
        else:  # Si el valor es mayor que el valor del nodo
            return self._buscar(valor, nodo.derecha)  # Buscar en el subárbol derecho
        cont=cont+1
        print('El contador de frecuencia o iteraciones: ',str(cont))



