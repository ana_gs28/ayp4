import tkinter as tk
from tkinter import messagebox
import matplotlib.pyplot as plt
import random
import numpy as np

def secuencia_aleatoria(n, a, b):
    secuencia = [random.randint(a, b) for _ in range(n)]
    return secuencia

def lineal(secuencia):
    return secuencia

def log_base2(secuencia):
    return np.log2(secuencia)

def semilog_base2(secuencia):
    return secuencia * np.log2(secuencia)

def log_base8(secuencia):
    return np.log(secuencia) / np.log(8)

def exponencial(secuencia):
    return 2 ** secuencia

def cuadratica(secuencia):
    return secuencia ** 2

def cubica(secuencia):
    return secuencia ** 3

def grafica_funciones(n,a,b):
    y=secuencia_aleatoria(n, a, b)
    x = np.arange(1, len(y) + 1)

    plt.figure(figsize=(12, 8))

    plt.plot(x,lineal(x), label='O(n)')
    plt.plot(x,log_base2(x), label='O(log2(n))')
    plt.plot(x,semilog_base2(x), label='O(nlog2(n))')
    plt.plot(x,log_base8(x), label='O(log8^n)')
    plt.plot(x,exponencial(x), label='O(2^n)')
    plt.plot(x,cuadratica(x), label='O(n^2)')
    plt.plot(x,cubica(x), label='O(n^3)')

    plt.xlabel('Índice de la secuencia')
    plt.ylabel('Valor de la función')
    plt.title('Orden de complejidad')
    plt.legend()
    plt.yscale('log')
    plt.show()

def generar_y_graficar():
    try:
        cantidad = int(entry_cantidad.get())
        limite_inferior = int(entry_limite_inferior.get())
        limite_superior = int(entry_limite_superior.get())
        
        if limite_inferior > limite_superior:
            raise ValueError("El límite inferior no puede ser mayor que el límite superior.")
        
        grafica_funciones(cantidad, limite_inferior, limite_superior)
        messagebox.showerror("Secuencia",secuencia_aleatoria(cantidad, limite_inferior, limite_superior))
    except ValueError as e:
        messagebox.showerror("Error", str(e))


# Botón para generar secuencia aleatoria
root = tk.Tk()
root.title("Análisis de Complejidad")

# Crear el frame para contener los botones
frame = tk.Frame(root)
frame.pack(pady=20)

# Ingresar cantidad de datos
label_cantidad = tk.Label(frame, text="Cantidad de datos:")
label_cantidad.pack(side=tk.LEFT)

entry_cantidad = tk.Entry(frame)
entry_cantidad.pack(side=tk.LEFT)

# Ingresar limite inferior
label_limite_inferior = tk.Label(frame, text="a límite inferior:")
label_limite_inferior.pack(side=tk.LEFT)

entry_limite_inferior = tk.Entry(frame)
entry_limite_inferior.pack(side=tk.LEFT)

# Ingresar limite superior
label_limite_superior = tk.Label(frame, text="b límite superior:")
label_limite_superior.pack(side=tk.LEFT)

entry_limite_superior = tk.Entry(frame)
entry_limite_superior.pack(side=tk.LEFT)

# Botón para generar y graficar
button_generar = tk.Button(frame, text="Generar y Graficar", command=generar_y_graficar)
button_generar.pack(side=tk.LEFT, padx=10)

# Iniciar el bucle principal de la interfaz
root.mainloop()
