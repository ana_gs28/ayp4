import random
import time as tm
n=int(input("Ingrese la cantidas de datos: "))
vectorA = [random.randint(1, 100) for _ in range(n)]
for i in range(n-1):
    indice = i
    j=i+1
    for j in range(i+1, n):
        if vectorA[j] < vectorA[indice]:
            indice = j
    temp = vectorA[i]
    vectorA[i] = vectorA[indice]
    vectorA[indice] = temp

print(vectorA)

k, h = 0,0
while k<n-1:
    indice = k
    h=k+1
    while h<n:
        if vectorA[h] < vectorA[indice]:
            indice = h
        h+=1
    temp = vectorA[k]
    vectorA[k] = vectorA[indice]
    vectorA[indice] = temp
    k+=1

print(vectorA)